@extends('layouts.master')

@section('titulo')
    Laravel Vacunacion
@endsection

@section('contenido')
    
    <form method="POST">
        @csrf
        <div class="row">
            @foreach($vacunas as $vacuna)
            <div class="col-xs-12 col-sm-6 col-md-4 ">
                <a href="{{ route('vacunas.show', $vacuna)}}">             
                        <h3 style="min-height:45px;margin:5px 0 10px 0">{{$vacuna->nombre}}</h3>
                        @php
                            $grupos=$vacuna->grupos;

                        @endphp
                        <h4>Posibles grupos de vacunación</h4>
                        <ul>
                            @php
                                foreach ($grupos as $grupo){
                                    echo "<li>". $grupo->nombre."</li>";
                                }
                            @endphp
                        </ul>

                        
                    </a>
                </div>
                <br>
            @endforeach
        </div>

    </form>
    
@endsection