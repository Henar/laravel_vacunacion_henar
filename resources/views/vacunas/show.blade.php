@extends('layouts.master')

@section('titulo')
    Laravel Vacunacion
@endsection

@section('contenido')

    @if (session ('mensaje'))
        <div class="alert alert-info">{{session('mensaje')}}</div>
    @endif

    @php
        $grupos=$vacuna->grupos;

    @endphp 
    <form method="POST">
        @csrf
        <h1 style="margin-left: 25%">{{$vacuna->nombre}}</h1>
        <div class="row">

            <div class="col-sm-3">
                <h3>Pacientes NO VACUNADOS:</h3>
                @php

                     echo '<table border="1">';
                     echo '<tr><th>Nombre</th><th>Grupo de vacunación</th>
                                <th>Prioridad</th><th>Acción</th></tr>';
                    foreach ($grupos as $grupo) {
                        $pacientes=$grupo->pacientes;
                       
                        foreach ($pacientes as $paciente) {
                            if($paciente->vacunado==false){
                                echo '<tr>'; 
                                    echo '<td>'. $paciente->nombre. '</td>';
                                    echo '<td>'. $grupo->nombre. '</td>';
                                    echo '<td>'. $grupo->prioridad. '</td>';
                                    echo '<td><a class="btn btn-success" href="pacientes.vacunar">Vacunar</a></td>';
                                    
                                echo '</tr>';
                                
                            }
                                
                        }
                        
                    }
                    echo '</table>';
                @endphp
                

                <h3>Pacientes VACUNADOS:</h3>
                @php
                    foreach ($grupos as $grupo) {
                        $pacientes=$grupo->pacientes;
                         echo '<div><b>'. $grupo->nombre. '</b>';
                        foreach ($pacientes as $paciente) {
                              
                                echo '<ul>';
                                    if($paciente->vacunado==true){
                                        echo '<li>'. $paciente->nombre. '('. $paciente->fechaVacuna. ')</li>';
                                   
                                    }
                                echo '</ul>';
                            
                        } 
                        echo '</div>';
                    }
                @endphp
            </div>
        </div>
    </form>
@endsection