<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PacienteController extends Controller
{
    public function vacunar($paci){
        $p=Paciente::findOrFail($paci);
        DB::table('pacientes')->where('id', $paci)->update(['vacunado' => true]);
        $nombre=DB::table('pacientes')->where('id', $paci)->pluck('nombre');
        return redirect()->route('vacunas.show', $p)->with('mensaje', $nombre. " se ha vacunado");
    }
}
