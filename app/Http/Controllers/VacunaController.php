<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use App\Models\Paciente;
use App\Models\Vacuna;
use Illuminate\Http\Request;

class VacunaController extends Controller
{
    public function index(){
        $vacunas=Vacuna::all();
		return view('vacunas.index', compact('vacunas'));
    }

    public function show(Vacuna $vacuna){
		$v=Vacuna::findOrFail($vacuna->id);
		return view('vacunas.show', ["vacuna"=>$v]);
    }


    public function crear(Request $request){
        // return $request->nombre;
        $v=new Vacuna();
        $v->nombre = $request->nombre;
        // return response()->json($v->save());
        if($v->save()){
            return response()->json(['mensaje' => "{$request->nombre} se ha insertado correctamente"]);
        }else{
            return response()->json(['mensaje' => "No se ha podido insertar la vacuna"]);
        }
    }

    // public function mostrar(Request $request){
    //     return $request;
    //     $p=Paciente::where('id', $request->idPaciente)->get();
    //     $grupo=$p->grupo;
    //     $vacunas=$grupo->vacunas;
    //     return $p;
    //     return response()->json($vacunas);
    // }
}
