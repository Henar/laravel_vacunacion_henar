<?php

namespace App\Http\Controllers;

use App\Models\Vacuna;
use Illuminate\Http\Request;


class InicioController extends Controller
{
    public function __invoke()
    {
        // return view('welcome');
        return redirect()->action([VacunaController::class, 'index']);
    }
}
