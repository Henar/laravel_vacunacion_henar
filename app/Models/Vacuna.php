<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacuna extends Model
{
    use HasFactory;
    protected $table='vacunas';

    public function getRouteKeyName()
    {
        return 'slug';
    }


    public function grupos(){
        return $this->belongsToMany(Grupo::class)->orderBy('prioridad', 'desc');
    }


}
